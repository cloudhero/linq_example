﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Example
{
    class ApplicationDetail
    {
        public string Name { get; set; }
        public int ApplicationID { get; set; }

        public ApplicationDetail(string name = "No Name")
        {
            Name = name;
        }
    }
}
