﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            QueryStringArray();
            QueryIntArray();
            QueryArrayList();
            QueryCollection();
            QueryFileData();
            Console.ReadLine();
        }

        static void QueryStringArray()
        {
            string[] fileDetails = {
                "packages.config", "details.json", "foo.xml", "newDetails.json", "blankFile", "mightyFile.txt", "app.config", "theClass.cs", ""
            };

            var fileQuery = from fileDetail in fileDetails
                            where fileDetail.Contains(".xml")
                            orderby fileDetail descending
                            select fileDetail;

            foreach (var i in fileQuery)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();
        }

        static int[] QueryIntArray()
        {
            //Demonstrates that LINQ queries are updated in real time when the source (array) is updated.
            int[] sizeArray = { 5, 10, 15, 20, 25, 30, 35 };

            var sizeQueryGT20 = from currSize in sizeArray
                       where currSize > 20
                       orderby currSize
                       select currSize;

            foreach (var i in sizeQueryGT20)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();

            Console.WriteLine($"Get Type : {sizeQueryGT20.GetType()}");

            var listGT20 = sizeQueryGT20.ToList<int>();
            var arrayGT20 = sizeQueryGT20.ToArray();

            sizeArray[0] = 40;

            foreach (var i in sizeQueryGT20)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine();
            return arrayGT20;
        }

        static void QueryArrayList()
        {
            ArrayList myFiles = new ArrayList()
            {
                    new FileDetail(
                        name: "test", 
                        sizeType: "GB", 
                        lastModified: DateTime.Now - (new TimeSpan(1, 0, 0, 0)), 
                        size: .8),

                    new FileDetail(
                        name: "test", 
                        sizeType: "GB", 
                        lastModified: DateTime.Now - (new TimeSpan(0, 2, 0, 0)), 
                        size: .8),

                    new FileDetail(
                        name: "test", 
                        sizeType: "GB", 
                        lastModified: DateTime.Now - (new TimeSpan(1, 0, 5, 0)), 
                        size: .8)
            };

            var myFilesEnum = myFiles.OfType<FileDetail>();
            var smallFiles = from file in myFilesEnum
                             where file.Size <= 90 && file.SizeType == "MB"
                           orderby file.Name
                           select file;

            foreach (var file in smallFiles)
            {
                Console.WriteLine("The file \"{0}\" size is {1}{2}.", file.Name, file.Size, file.SizeType);
            }

            Console.WriteLine();
        }

        static void QueryCollection()
        {
            var fileList = new List<FileDetail>()
            {
                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(1, 0, 0, 0)),
                    size: .8),

                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(0, 2, 0, 0)),
                    size: .8),

                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(1, 0, 5, 0)),
                    size: .8)
            };

            var bigFiles = from file in fileList
                          where ((file.Size > 70)) && (file.Size > 25)
                          orderby file.Name
                          select file;

            foreach (var file in bigFiles)
            {
                Console.WriteLine("The file \"{0}\" size is {1}{2}.", file.Name, file.Size, file.SizeType);
            }

            Console.WriteLine();
        }

        static void QueryFileData()
        {
            //Query of file names and lastModified date
            #region
            var fileList = new List<FileDetail>()
            {
                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(1, 0, 0, 0)),
                    size: .8)
                {
                    FileID = 1
                },

                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(0, 2, 0, 0)),
                    size: .8)
                {
                    FileID = 2
                },

                new FileDetail(
                    name: "test",
                    sizeType: "GB",
                    lastModified: DateTime.Now - (new TimeSpan(1, 0, 5, 0)),
                    size: .8)
                {
                    FileID = 1
                }
            };

            ApplicationDetail[] applications = new[]
            {
                new ApplicationDetail
                {
                    Name = "TheNewShirt",
                    ApplicationID = 1
                },

                new ApplicationDetail
                {
                    Name = "AnotherReleasedThingy",
                    ApplicationID = 2
                },

                new ApplicationDetail
                {
                    Name = "WooptyZippityZoo",
                    ApplicationID = 3
                }
            };

            //Lists the files with Name and LastModified properties
            var nameLastModified = from a in fileList
                                   select new
                             {
                                 a.Name,
                                 a.LastModified
                             };

            foreach (var i in nameLastModified)
            {
                Console.WriteLine(i.ToString());
            }

            Console.WriteLine();
            #endregion

            //Demonstrates a join query between files and applications data
            #region
            var innerJoin =
                from file in fileList
                join application in applications on file.FileID
                equals application.ApplicationID
                select new
                {
                    ApplicationName = application.Name,
                    FileName = file.Name
                };

            foreach (var i in innerJoin)
            {
                Console.WriteLine("{0} contains file {1}.", i.ApplicationName, i.FileName);
            }


            Console.WriteLine();
            #endregion

            //Demonstrates nested query, building a temp table in nested query
            #region
            var groupJoin = from application in applications
                            orderby application.ApplicationID
                            join file in fileList
                            on application.ApplicationID
                            equals file.FileID
                            into applicationGroup
                            select new
                            {
                                Application = application.Name,
                                Files = from applicationTemp
                                          in applicationGroup
                                          orderby applicationTemp.Name
                                          select applicationTemp
                            };
            
            foreach (var applicationGroup in groupJoin)
            {
                Console.WriteLine(applicationGroup.Application);
                foreach (var file in applicationGroup.Files)
                {
                    Console.WriteLine("* {0}", file.Name);
                }
            }
            #endregion
        }
    }
}
