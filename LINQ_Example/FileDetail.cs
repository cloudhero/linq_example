﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_Example
{
    class FileDetail
    {
        public string Name { get; set; }
        public double Size { get; set; }
        public string SizeType { get; set; }
        public DateTime LastModified { get; set; }
        public int FileID { get; set; }

        public FileDetail(string name, string sizeType, DateTime lastModified, double size = 0)
        {
            Name = name;
            Size = size;
            SizeType = sizeType;
            LastModified = LastModified;
        }

        public override string ToString()
        {
            return string.Format("{0} is {1}{2} and was last modified on {3}.", Name, Size, SizeType, LastModified);
        }
    }
}
